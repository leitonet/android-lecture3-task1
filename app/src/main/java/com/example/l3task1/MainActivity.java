package com.example.l3task1;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText inputTime;
    Button start;
    Button pause;
    MyTimer myTimer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inputTime = (EditText) findViewById(R.id.input_time);

        start = (Button) findViewById(R.id.start);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 myTimer = (MyTimer) new MyTimer(Integer.parseInt(inputTime.getText().toString())).execute();
            }
        });

        pause = (Button) findViewById(R.id.pause);
        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myTimer.cancel(true);
            }
        });
    }

    class MyTimer extends AsyncTask<Integer, Integer, Void>{

        int seconds = 0;

        public MyTimer(int seconds) {
            this.seconds = seconds;
        }


        @Override
        protected Void doInBackground(Integer... ints) {
            while(true) {
                if (!(seconds <= 0)) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    seconds--;
                    publishProgress(seconds);
                }else {
                    break;
                }
            }

            return null;

        }

        @Override
        protected void onPreExecute() {
            inputTime.setText(seconds + "");
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Toast.makeText(MainActivity.this, "Time has left, please input next time!", Toast.LENGTH_SHORT).show();
            inputTime.setText("");
            inputTime.setHint("Time has left, please input next time!");
        }

        @Override
        protected void onProgressUpdate(Integer... ints) {
            inputTime.setText(ints[0] + "");
        }
    }


}


